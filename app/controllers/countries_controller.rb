class CountriesController < ApplicationController
  def new
    respond_to do |format|
      format.html 
      format.rdf {render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)}
      format.xml {render status: :ok, xml: repo.dump(:rdfxml, standard_prefixes: true)}
      format.json {render status: :ok, json: repo.dump(:jsonld, standard_prefixes: false)}
    end
  end

  def create
  end

  def index
    countries_vocabulary  = Vocabulary.country

    @result = []

    if !params[:country].blank?
      Country.any_of({subject: /#{countries_vocabulary[params[:country]]}/i}
      ).each do |row|
          @result << row.subject
      end
    end

    respond_to do |format|
      format.html { @result }
      format.rdf {render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)}
      format.xml {render status: :ok, xml: repo.dump(:rdfxml, standard_prefixes: true)}
      format.json {render status: :ok, json: repo.dump(:jsonld, standard_prefixes: false)}
    end
  end

  def show
    products_vocabulary  = Vocabulary.product
    countries_vocabulary  = Vocabulary.country
    allergens_vocabulary  = Vocabulary.allergen
    ontologies_vocabulary  = Vocabulary.ontology
    properties_vocabulary  = Vocabulary.property

    repo = RDF::Repository.new
    repo << RDF::Statement.new(RDF::URI.new(""), RDF.type, RDF::OWL.Ontology)
    repo << RDF::Statement.new(RDF::URI.new(""), RDF::RDFS.comment, "Country #{params[:country]}")

    Country.where(subject: countries_vocabulary[params[:country]]).each do |row|
      if row.predicate == RDF.type
        repo << RDF::Statement.new(RDF::URI.new(row.subject), RDF::URI.new(row.predicate), RDF::URI.new(row.object))
        repo << RDF::Statement.new(RDF::URI.new(""), RDF::OWL.imports, RDF::URI.new("#{row.object}.rdf"))
      else
        repo << RDF::Statement.new(RDF::URI.new(row.subject), RDF::URI.new(row.predicate), row.object)
      end
    end

    respond_to do |format|
      format.html do
        @result = Hash.from_xml(repo.dump(:rdfxml, standard_prefixes: true))
      end
      #format.html {render status: :ok, body: repo.dump(:rdfa, standard_prefixes: true)}
      format.rdf {render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)}
      format.xml {render status: :ok, xml: repo.dump(:rdfxml, standard_prefixes: true)}
      format.json {render status: :ok, json: repo.dump(:jsonld, standard_prefixes: false)}
    end
  end
end
