class OntologiesController < ApplicationController
  def show
    ontologies_vocabulary  = Vocabulary.ontology

    repo = RDF::Repository.new

    Ontology.where(subject: ontologies_vocabulary[params[:ontology]]).each do |row|
      repo << RDF::Statement.new(RDF::URI.new(row.subject), RDF::URI.new(row.predicate), RDF::URI.new(row.object))
    end

    respond_to do |format|
      format.html do
        @result = Hash.from_xml(repo.dump(:rdfxml, standard_prefixes: true))
      end
      format.rdf {render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)}
      format.xml {render status: :ok, xml: repo.dump(:rdfxml, standard_prefixes: true)}
      format.json {render status: :ok, json: repo.dump(:jsonld, standard_prefixes: true)}
    end
  end
end
