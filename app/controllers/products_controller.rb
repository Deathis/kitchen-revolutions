class ProductsController < ApplicationController
  def new
    @countries = CSV.read('./data/countries.csv',headers: true,).to_a.drop(1)
    @allergens = CSV.read('./data/allergens.csv',headers: true,).to_a.drop(1)

    respond_to do |format|
      format.html 
      format.rdf {render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)}
      format.xml {render status: :ok, xml: repo.dump(:rdfxml, standard_prefixes: true)}
      format.json {render status: :ok, json: repo.dump(:jsonld, standard_prefixes: false)}
    end
  end

  def create
    products_vocabulary = Vocabulary.product
    countries_vocabulary = Vocabulary.country
    allergens_vocabulary = Vocabulary.allergen
    ontologies_vocabulary = Vocabulary.ontology
    properties_vocabulary = Vocabulary.property

    @countries = CSV.read('./data/countries.csv',headers: true,).to_a.drop(1)
    @allergens = CSV.read('./data/allergens.csv',headers: true,).to_a.drop(1)

    if !params[:name].nil? and !params[:county].nil? and !params[:allergens].nil? and !params[:trace].nil?
      Product.new(subject: products_vocabulary[params[:name].capitalize], predicate: RDF.type, object: ontologies_vocabulary['Product']).save!
      Product.new(subject: products_vocabulary[params[:name].capitalize], predicate: properties_vocabulary['name'], object: params[:name]).save!
      Product.new(subject: products_vocabulary[params[:name].capitalize], predicate: properties_vocabulary['traces'], object: params[:trace]).save!

      params[:county].each do |country|
        Product.new(subject: products_vocabulary[params[:name].capitalize],
                    predicate: properties_vocabulary["from"],
                    object: countries_vocabulary[country]).save!
      end

      params[:allergens].each do |allergen|
        Product.new(subject: products_vocabulary[params[:name].capitalize],
                    predicate: properties_vocabulary["hasAllergen"],
                    object: allergens_vocabulary[allergen]).save!
      end

      redirect_to :resources_products
    else
      render :new
    end
  end

  def index
    products_vocabulary  = Vocabulary.product
    countries_vocabulary  = Vocabulary.country
    allergens_vocabulary  = Vocabulary.allergen
    ontologies_vocabulary  = Vocabulary.ontology
    properties_vocabulary  = Vocabulary.property

    @result = []
    products = []

    products << Product.where(predicate: properties_vocabulary['name'], object: /#{params[:name]}/i).pluck(:subject).uniq if params[:name].present?
    if params[:country].present?
      if products.any?
        products << Product.where(predicate: properties_vocabulary['from'], object: /#{countries_vocabulary[params[:country]]}/i).pluck(:subject).uniq
        products = products.flatten.group_by{ |e| e }.select { |k, v| v.size > 1 }.map(&:first)
      else
        products << Product.where(predicate: properties_vocabulary['from'], object: /#{countries_vocabulary[params[:country]]}/i).pluck(:subject).uniq
      end
    end
    if params[:hasAllergen].present?
      if products.any?
        products << Product.where(predicate: properties_vocabulary['hasAllergen'], object: /#{allergens_vocabulary[params[:hasAllergen]]}/i).pluck(:subject).uniq
        products = products.flatten.group_by{ |e| e }.select { |k, v| v.size > 1 }.map(&:first)
      else
        products << Product.where(predicate: properties_vocabulary['hasAllergen'], object: /#{allergens_vocabulary[params[:hasAllergen]]}/i).pluck(:subject).uniq
      end
    end

    @result = products.flatten

    respond_to do |format|
      format.html { @result }
      format.rdf {render status: :ok, xml: imports(@result).dump(:rdf, standard_prefixes: true)}
      format.xml {render status: :ok, xml: imports(@result).dump(:rdfxml, standard_prefixes: true)}
      format.json {render status: :ok, json: imports(@result).dump(:jsonld, standard_prefixes: false)}
    end
  end

  def imports(products)
    repo = RDF::Repository.new
    repo << RDF::Statement.new(RDF::URI.new(""), RDF.type, RDF::OWL.Ontology)
    repo << RDF::Statement.new(RDF::URI.new(""), RDF::RDFS.comment, "Product Resources")

    products.each do |product|
      repo << RDF::Statement.new(RDF::URI.new(""), RDF::OWL.imports, RDF::URI.new("#{product}.rdf"))
    end
    repo
  end

  def show
    products_vocabulary  = Vocabulary.product
    countries_vocabulary  = Vocabulary.country
    allergens_vocabulary  = Vocabulary.allergen
    ontologies_vocabulary  = Vocabulary.ontology
    properties_vocabulary  = Vocabulary.property

    product_properties = [
      "code",
      "url",
      "name",
      "purchase_places",
      "traces",
      "serving_size",
      "additives_en",
      "pnns_groups_1",
      "pnns_groups_2",
      "image_url",
      "image_small_url",
      "hasAllergen",
      "from",
      "isSoldIn",
      "energy_100g",
      "fat_100g"
      
    ]

    repo = RDF::Repository.new

    repo << RDF::Statement.new(RDF::URI.new(""), RDF.type, RDF::OWL.Ontology)
    repo << RDF::Statement.new(RDF::URI.new(""), RDF::RDFS.comment, "Product #{params[:code]}")

    product_properties.each do |property|
      repo << RDF::Statement.new(RDF::URI.new(""), RDF::OWL.imports, properties_vocabulary["#{property}.rdf"])
    end

  ["Product", "Country", "Allergen", "Store"].each do |c|
      repo << RDF::Statement.new(RDF::URI.new(""), RDF::OWL.imports, ontologies_vocabulary["#{c}.rdf"])
    end

    Product.where(subject: products_vocabulary[params[:code]]).each do |row|
      if row.predicate == properties_vocabulary['from'] || row.predicate == properties_vocabulary['hasAllergen'] || row.predicate == RDF.type || row.predicate == properties_vocabulary['isSoldIn']
        repo << RDF::Statement.new(RDF::URI.new(row.subject), RDF::URI.new(row.predicate), RDF::URI.new(row.object))
        repo << RDF::Statement.new(RDF::URI.new(""), RDF::OWL.imports, RDF::URI.new("#{row.object}.rdf"))
      else
        repo << RDF::Statement.new(RDF::URI.new(row.subject), RDF::URI.new(row.predicate), row.object)
      end
    end

    respond_to do |format|
      format.html do
        @result = Hash.from_xml(repo.dump(:rdfxml, standard_prefixes: true))
      end
      format.rdf {render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)}
      format.xml {render status: :ok, xml: repo.dump(:rdfxml, standard_prefixes: true)}
      format.json {render status: :ok, json: repo.dump(:jsonld, standard_prefixes: true)}
    end
  end
end
