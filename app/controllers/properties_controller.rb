class PropertiesController < ApplicationController
  def show
    products_vocabulary  = Vocabulary.product
    countries_vocabulary  = Vocabulary.country
    allergens_vocabulary  = Vocabulary.allergen
    ontologies_vocabulary  = Vocabulary.ontology
    properties_vocabulary  = Vocabulary.property
    rdf_vocabulary = RDF::Vocabulary.new("http://www.w3.org/1999/02/22-rdf-syntax-ns#")

    repo = RDF::Repository.new
    repo << RDF::Statement.new(RDF::URI.new(""), RDF.type, RDF::OWL.Ontology)
    repo << RDF::Statement.new(RDF::URI.new(""), RDF::RDFS.comment, "Property #{params[:property]}")

    repo << RDF::Statement.new(properties_vocabulary[params[:property]], RDF.type, rdf_vocabulary['Property'])
    
    repo << RDF::Statement.new(properties_vocabulary[params[:property]], RDF::RDFS.label, params[:property])

    Property.where(subject: properties_vocabulary[params[:property]]).each do |row|
      repo << RDF::Statement.new(RDF::URI.new(row.subject), RDF::URI.new(row.predicate), RDF::URI.new(row.object))
    end

    respond_to do |format|
      format.html do
        @result = Hash.from_xml(repo.dump(:rdfxml, standard_prefixes: true))
      end
      #format.html {render status: :ok, body: repo.dump(:rdfa, standard_prefixes: true)}
      format.rdf {render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)}
      format.xml {render status: :ok, xml: repo.dump(:rdfxml, standard_prefixes: true)}
      format.json {render status: :ok, json: repo.dump(:jsonld, standard_prefixes: false)}
    end
  end
end
