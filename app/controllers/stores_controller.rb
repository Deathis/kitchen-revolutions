class StoresController < ApplicationController
  def index
    stores_vocabulary  = Vocabulary.store

    @result = []

    if !params[:isSoldIn].blank?
      Store.any_of({subject: /#{stores_vocabulary[params[:isSoldIn]]}/i}
      ).each do |row|
          @result << row.subject
      end
    end

    repo = RDF::Repository.new
    repo << RDF::Statement.new(RDF::URI.new(""), RDF.type, RDF::OWL.Ontology)
    repo << RDF::Statement.new(RDF::URI.new(""), RDF::RDFS.comment, "Store Resources")

    Store.where(predicate: RDF.type).each do |row|
      repo << RDF::Statement.new(RDF::URI.new(""), RDF::OWL.imports, RDF::URI.new("#{row.subject}.rdf"))
    end

    respond_to do |format|
      format.html { @result }
      format.html {render status: :ok, body: repo.dump(:rdfa, standard_prefixes: true)}
      format.rdf {render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)}
      format.xml {render status: :ok, xml: repo.dump(:rdfxml, standard_prefixes: true)}
      format.json {render status: :ok, json: repo.dump(:jsonld, standard_prefixes: false)}
    end
  end

  def show
    stores_vocabulary  = Vocabulary.store

    repo = RDF::Repository.new
    repo << RDF::Statement.new(RDF::URI.new(""), RDF.type, RDF::OWL.Ontology)
    repo << RDF::Statement.new(RDF::URI.new(""), RDF::RDFS.comment, "Allergen Ontology")

    Store.where(subject: stores_vocabulary[params[:store]]).each do |row|
      if row.predicate == RDF.type
        repo << RDF::Statement.new(RDF::URI.new(row.subject), RDF::URI.new(row.predicate), RDF::URI.new(row.object))
        repo << RDF::Statement.new(RDF::URI.new(""), RDF::OWL.imports, RDF::URI.new("#{row.object}.rdf"))
      else
        repo << RDF::Statement.new(RDF::URI.new(row.subject), RDF::URI.new(row.predicate), row.object)
      end
    end

    respond_to do |format|
      format.html do
        @result = Hash.from_xml(repo.dump(:rdfxml, standard_prefixes: true))
      end
      format.rdf {render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)}
      format.xml {render status: :ok, xml: repo.dump(:rdfxml, standard_prefixes: true)}
      format.json {render status: :ok, json: repo.dump(:jsonld, standard_prefixes: false)}
    end
  end
end
