class TestController < ApplicationController
  def ontology
    ontology = RDF::Vocabulary.new("http://#{request.host}/ontology/")

    repo = RDF::Repository.new

    repo << RDF::Statement.new(ontology['XXX'], RDF.type, RDF::OWL.Class)
    repo << RDF::Statement.new(ontology['Person'], RDF.type, RDF::OWL.Class)
    repo << RDF::Statement.new(ontology['Person'], RDF::RDFS.subClassOf, ontology['XXX'])

    render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)
  end

  def resource
    #<owl:Ontology rdf:about="http://www.co-ode.org/ontologies/pizza/2005/10/18/pizza.owl">
    #    <owl:imports rdf:resource="http://protege.stanford.edu/plugins/owl/protege"/>
    #</owl:Ontology>
    
    
    ontology = RDF::Vocabulary.new("http://#{request.host}/ontology/")
    resource = RDF::Vocabulary.new("http://#{request.host}/resource/")
    property = RDF::Vocabulary.new("http://#{request.host}/property/")

    repo = RDF::Repository.new

    repo << RDF::Statement.new(resource['JanKowalski'], RDF.type, ontology['Person'])
    repo << RDF::Statement.new(resource['JanKowalski'], property['name'], "Jan Kowalski")
    
    repo << RDF::Statement.new(ontology['Person'], RDF.type, RDF::OWL.Ontology)
    repo << RDF::Statement.new(ontology['Person'], RDF::OWL.imports, ontology['Person.rdf'])
    repo << RDF::Statement.new(property['name'], RDF.type, RDF::OWL.Ontology)
    repo << RDF::Statement.new(property['name'], RDF::OWL.imports, property['name.rdf'])

    respond_to do |format|
      format.html {render status: :ok, body: repo.dump(:rdfa, standard_prefixes: true)}
      format.rdf {render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)}
      format.xml {render status: :ok, xml: repo.dump(:rdfxml, standard_prefixes: true)}
      format.json {render status: :ok, json: repo.dump(:jsonld, standard_prefixes: true)}
    end
  end
  
  def property
    property = RDF::Vocabulary.new("http://#{request.host}/property/")
    rdf = RDF::Vocabulary.new("http://www.w3.org/1999/02/22-rdf-syntax-ns#")

    repo = RDF::Repository.new

    repo << RDF::Statement.new(property['name'], RDF.type, rdf['Property'])
    repo << RDF::Statement.new(property['name'], RDF.type, RDF::OWL.DatatypeProperty)

    render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)
  end

  def pawel
    resource = RDF::Vocabulary.new("http://#{request.host}/resource/")
    repo = RDF::Repository.new

    repo << RDF::Statement.new(RDF::FOAF.knows, RDF.type, RDF::OWL.ObjectProperty)
    repo << RDF::Statement.new(RDF::DC.title, RDF.type, RDF::OWL.DatatypeProperty)
    repo << RDF::Statement.new(RDF::FOAF.Person, RDF.type, RDF::OWL.Class)

    repo << RDF::Statement.new(resource['PawelPuchalski'], RDF.type, RDF::FOAF.Person)
    repo << RDF::Statement.new(resource['PawelPuchalski'], RDF::DC.title, "Pawel")
    repo << RDF::Statement.new(resource['PawelPuchalski'], RDF::FOAF.knows, resource['MiloszOsinski'])
    repo << RDF::Statement.new(resource['MiloszOsinski'], RDF.type, RDF::FOAF.Person)
    repo << RDF::Statement.new(resource['PawelPuchalski'], RDF::FOAF.knows, resource['JacekGalka'])
    repo << RDF::Statement.new(resource['JacekGalka'], RDF.type, RDF.Resource)
    #repo << milosz

    render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)
  end

  def milosz
    resource = RDF::Vocabulary.new("http://#{request.host}/resource/")

    repo = RDF::Repository.new

    repo << RDF::Statement.new(RDF::FOAF.knows, RDF.type, RDF::OWL.ObjectProperty)
    repo << RDF::Statement.new(RDF::DC.title, RDF.type, RDF::OWL.DatatypeProperty)
    repo << RDF::Statement.new(RDF::FOAF.Person, RDF.type, RDF::OWL.Class)

    repo << RDF::Statement.new(resource['MiloszOsinski'], RDF.type, RDF::FOAF.Person)
    repo << RDF::Statement.new(resource['MiloszOsinski'], RDF::DC.title, "Milosz")
    repo << RDF::Statement.new(resource['MiloszOsinski'], RDF::FOAF.knows, resource['JacekGalka'])
    repo << RDF::Statement.new(resource['MiloszOsinski'], RDF::FOAF.knows, resource['PawelPuchalski'])
    repo
    render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)
  end

  def jacek
    resource = RDF::Vocabulary.new("http://#{request.host}/resource/")

    repo = RDF::Repository.new

    repo << RDF::Statement.new(RDF::FOAF.knows, RDF.type, RDF::OWL.ObjectProperty)
    repo << RDF::Statement.new(RDF::DC.title, RDF.type, RDF::OWL.DatatypeProperty)
    repo << RDF::Statement.new(RDF::FOAF.Person, RDF.type, RDF::OWL.Class)

    repo << RDF::Statement.new(resource['JacekGalka'], RDF.type, RDF::FOAF.Person)
    repo << RDF::Statement.new(resource['JacekGalka'], RDF::DC.title, "Jacek")
    repo << RDF::Statement.new(resource['JacekGalka'], RDF::FOAF.knows, resource['PawelPuchalski'])
    repo << RDF::Statement.new(resource['JacekGalka'], RDF::FOAF.knows, resource['MiloszOsinski'])

    render status: :ok, xml: repo.dump(:rdf, standard_prefixes: true)
  end
end
