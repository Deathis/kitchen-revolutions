module ApplicationHelper
  def auto_html(text)
    text = text.gsub( %r{http://[^\s<]+} ) do |url|
      if url[/(?:png|jpe?g|gif|svg)$/]
        "<img src='#{url}' />"
      else
        "<a href='#{url}'>#{url}</a>"
      end
    end
    return text.html_safe
  end
end
