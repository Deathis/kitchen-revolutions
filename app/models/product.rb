class Product
  include Mongoid::Document
  field :subject, type: String
  field :predicate, type: String
  field :object, type: String

  def self.categories
    CSV.read('./data/categories.csv',headers: true,).to_a.drop(1).map{|c| c[0].split(' ').collect(&:capitalize).join}
  end
end
