class Store
  include Mongoid::Document
  field :subject, type: String
  field :predicate, type: String
  field :object, type: String
end
