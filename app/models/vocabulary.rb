class Vocabulary
  @@hostname = Figaro.env.domain

  def self.product
    RDF::Vocabulary.new("#{@@hostname}/resources/products/")
  end

  def self.country
    RDF::Vocabulary.new("#{@@hostname}/resources/countries/")
  end

  def self.allergen
    RDF::Vocabulary.new("#{@@hostname}/resources/allergens/")
  end

  def self.property
    RDF::Vocabulary.new("#{@@hostname}/properties/")
  end

  def self.ontology
    RDF::Vocabulary.new("#{@@hostname}/ontologies/")
  end

  def self.store
    RDF::Vocabulary.new("#{@@hostname}/resources/stores/")
  end
end
