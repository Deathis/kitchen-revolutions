Rails.application.routes.draw do
  get 'resources/products', to: 'products#index'
  get 'resources/products/new', to: 'products#new'
  post 'resources/products/create', to: 'products#create'
  get 'resources/products/:code', to: 'products#show'

  get 'resources/countries', to: 'countries#index'
  get 'resources/countries/new', to: 'countries#new'
  post 'resources/countries/create', to: 'countries#create'
  get 'resources/countries/:country', to: 'countries#show'

  get 'resources/allergens', to: 'allergens#index'
  get 'resources/allergens/:allergen', to: 'allergens#show'

  get 'resources/stores', to: 'stores#index'
  get 'resources/stores/:store', to: 'stores#show'

  get 'properties/:property', to: 'properties#show'
  get 'ontologies/:ontology', to: 'ontologies#show'

  root 'products#index'
end
