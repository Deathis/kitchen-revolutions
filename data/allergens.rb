require 'csv'

allergens = ["allergens"]

CSV.foreach('products.csv', headers: true, col_sep: "\t", header_converters: :symbol) do |row|
  next if row[:allergens].nil?
  
  row[:allergens].split(',').each do |allergen|
    allergens << allergen unless allergens.include?(allergen)
  end
end

File.open("allergens.csv", 'w') do |f|
  allergens.each do |allergen|
    f.write(allergen + "\n")
  end
end
