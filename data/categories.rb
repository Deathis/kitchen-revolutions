require 'csv'

categories = ["categories"]

CSV.foreach('products.csv', headers: true, col_sep: "\t", header_converters: :symbol) do |row|
  next if row[:main_category_en].nil?
  
  row[:main_category_en].split(',').each do |category|
    categories << category unless categories.include?(category)
  end
end

File.open("categories.csv", 'w') do |f|
  categories.each do |category|
    f.write(category + "\n")
  end
end
