require 'csv'

countries = ["countries"]

CSV.foreach('products.csv', headers: true, col_sep: "\t", header_converters: :symbol) do |row|
  next if row[:countries_en].nil?
  
  row[:countries_en].split(',').each do |country|
    countries << country unless countries.include?(country)
  end
end

File.open("countries.csv", 'w') do |f|
  countries.each do |country|
    f.write(country + "\n")
  end
end
