require 'csv'

columns = [:code, :url, :product_name, :purchase_places, :countries_en, :allergens, :traces, :serving_size, :additives_en, :pnns_groups_1, :pnns_groups_2, :main_category_en, :image_url, :image_small_url, :stores, :energy_100g, :fat_100g]

data = [columns.join("\t")]

records = 0

CSV.foreach('en.openfoodfacts.org.products.fixed.csv', headers: true, col_sep: "\t", header_converters: :symbol) do |row|
  next if row[:product_name] !~ /^[\w\s]+$/
  next if row[:countries_en] !~ /^[\w\s,]*$/
  next if row[:allergens] !~ /^[\w\s,]*$/
  next if row[:main_category_en] !~ /^[\w\s,]*$/
  next if row[:stores] !~ /^[\w\s,]*$/

  row[:traces] = row[:traces].split(',').map(&:strip).map(&:capitalize).join(',') if row[:traces]
  row[:allergens] = row[:allergens].split(',').map(&:strip).map(&:capitalize).join(',') if row[:allergens]
  row[:countries_en] = row[:countries_en].split(',').map(&:strip).join(',') if row[:countries_en]
  row[:stores] = row[:stores].split(',').map(&:strip).map(&:capitalize).join(',') if row[:stores]

  data << columns.map {|column| row[column]}.join("\t")

  records = records + 1
end

puts records

File.open("products.csv", 'w') do |f|
  data.each do |row|
    f.write(row + "\n")
  end
end
