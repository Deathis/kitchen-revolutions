require 'csv'

stores = ["stores"]

CSV.foreach('products.csv', headers: true, col_sep: "\t", header_converters: :symbol) do |row|
  next if row[:stores].nil?
  
  row[:stores].split(',').each do |store|
    stores << store unless stores.include?(store)
  end
end

File.open("stores.csv", 'w') do |f|
  stores.each do |store|
    f.write(store.capitalize + "\n")
  end
end
