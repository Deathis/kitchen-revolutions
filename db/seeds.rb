product_datatype_properties = {
  code: "code",
  url: "url",
  product_name: "name",
  purchase_places: 'purchase_places',
  traces: "traces",
  serving_size: "serving_size",
  additives_en: "additives_en",
  pnns_groups_1: "pnns_groups_1",
  pnns_groups_2: "pnns_groups_2",
  image_url: "image_url",
  image_small_url: "image_small_url",
  energy_100g: 'energy_100g',
  fat_100g: 'fat_100g'
}

object_properties = {countries_en: "from", allergens: "hasAllergen", store: "isSoldIn"}

products_vocabulary = Vocabulary.product
countries_vocabulary = Vocabulary.country
allergens_vocabulary = Vocabulary.allergen
ontologies_vocabulary = Vocabulary.ontology
properties_vocabulary = Vocabulary.property
stores_vocabulary = Vocabulary.store

["Product", "Country", "Allergen", "Store"].each do |f|
  Ontology.new(subject: ontologies_vocabulary[f], predicate: RDF.type, object: RDF::OWL.Class).save!
end

CSV.foreach('./data/categories.csv', headers: true, col_sep: "\t", header_converters: :symbol) do |row|
  category = row[:categories].split(' ').collect(&:capitalize).join
  Ontology.new(subject: ontologies_vocabulary[category], predicate: RDF.type, object: RDF::OWL.Class).save!
  Ontology.new(subject: ontologies_vocabulary[category], predicate: RDF::RDFS.subClassOf, object: ontologies_vocabulary['Product']).save!
end

product_datatype_properties.each do |key, value|
  Property.new(subject: properties_vocabulary[value], predicate: RDF.type, object: RDF::OWL.DatatypeProperty).save!
end

object_properties.each do |key, value|
  Property.new(subject: properties_vocabulary[value], predicate: RDF.type, object: RDF::OWL.ObjectProperty).save!
end

Property.new(subject: properties_vocabulary['from'], predicate: RDF::RDFS.range, object: ontologies_vocabulary['Country']).save!
Property.new(subject: properties_vocabulary['hasAllergen'], predicate: RDF::RDFS.range, object: ontologies_vocabulary['Allergen']).save!
Property.new(subject: properties_vocabulary['isSoldIn'], predicate: RDF::RDFS.range, object: ontologies_vocabulary['Store']).save!

CSV.foreach('./data/products.csv', headers: true, col_sep: "\t", header_converters: :symbol) do |row|
  product_name = row[:product_name].split(' ').collect(&:capitalize).join
  category = row[:main_category_en].split(' ').collect(&:capitalize).join

  Product.new(subject: products_vocabulary[product_name], predicate: RDF.type, object: ontologies_vocabulary[category]).save!

  product_datatype_properties.each do |key, value|
    Product.new(subject: products_vocabulary[product_name], predicate: properties_vocabulary[value], object: row[key]).save! if row[key]
  end

  if row[:countries_en]
    row[:countries_en].split(',').each do |country|
      country = country.split(' ').collect(&:capitalize).join
      Product.new(subject: products_vocabulary[product_name], predicate: properties_vocabulary["from"], object: countries_vocabulary[country]).save!
    end
  end

  if row[:allergens]
    row[:allergens].split(',').each do |allergen|
      allergen = allergen.split(' ').collect(&:capitalize).join
      Product.new(subject: products_vocabulary[product_name], predicate: properties_vocabulary["hasAllergen"], object: allergens_vocabulary[allergen]).save!
    end
  end

  if row[:stores]
    row[:stores].split(',').each do |store|
      store = store.split(' ').collect(&:capitalize).join
      Product.new(subject: products_vocabulary[product_name], predicate: properties_vocabulary["isSoldIn"], object: stores_vocabulary[store]).save!
    end
  end
end

CSV.foreach('./data/allergens.csv', headers: true, col_sep: "\t", header_converters: :symbol) do |row|
  allergen = row[:allergens].split(' ').collect(&:capitalize).join
  Allergen.new(subject: allergens_vocabulary[allergen], predicate: RDF.type, object: ontologies_vocabulary['Allergen']).save!
end

CSV.foreach('./data/countries.csv', headers: true, col_sep: "\t", header_converters: :symbol) do |row|
  country = row[:countries].split(' ').collect(&:capitalize).join
  Country.new(subject: countries_vocabulary[country], predicate: RDF.type, object: ontologies_vocabulary['Country']).save!
end

CSV.foreach('./data/stores.csv', headers: true, col_sep: "\t", header_converters: :symbol) do |row|
  store = row[:stores].split(' ').collect(&:capitalize).join
  Store.new(subject: stores_vocabulary[store], predicate: RDF.type, object: ontologies_vocabulary['Store']).save!
end
